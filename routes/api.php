<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::post('/auth')->middleware('base_api_throttle:1000,1')->uses('Api\V1\Auth@createJwt');
Route::get('/auth')->middleware('base_api_throttle:1000,1')->uses('Api\V1\Auth@checkJwtAndSubscription');
