<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\Exception\ExceptionFormatter;
use App\Http\Controllers\Subscription\SubscriptionClient;
use Illuminate\Routing\Controller as BaseController;
use Ahc\Jwt\JWT;
use Validator;
use Log;



class Auth extends BaseController
{

    private $exceptionFormatter;

    public function createJwt(Request $request){

        try{

            //Validate the request
            $parameters = [
                'id_company' => 'required|string|size:10',
                'id_user' => 'required|string|size:10'
            ];
            $validation = Validator::make($request->all(), $parameters);
            if ($validation->fails()) {
                $errStr = json_encode($validation->failed());
                Log::error($errStr);
                return $this->sendErrorMessage(5200);
            }

            $claims = [
                'iss'     => 'AUTH_MISTRAL',
                'aud'     => 'MISTRAL_API',
                'sub'     => $request["id_user"],
                'id_company' =>  $request["id_company"]
            ];

            $jwt = new JWT(env("JWT_SECRET"), 'HS256', 864000, 0);
            $token = $jwt->encode($claims);
            return response()->json(["token" => $token] , 200);

        } catch (\Exception $e) {
        Log::error($e);
            return $this->sendErrorMessage(5300);
        }

    }

    public function checkJwtAndSubscription(Request $request)
    {

        try {

            $parameters = [
                'token' => 'required|string'
            ];

            $validation = Validator::make($request->all(), $parameters);
            if ($validation->fails()) {
                $errStr = json_encode($validation->failed());
                Log::error($errStr);
                return $this->sendErrorMessage(5200);
            }

            $jwt = new JWT(env("JWT_SECRET"), 'HS256', 864000, 0);
            $payload = $jwt->decode($request["token"]);

            $subscriptionClient = new SubscriptionClient();
            $hasValidSub = $subscriptionClient->hasValidSubscription($payload["id_company"]);

            if($hasValidSub) return response()->json(["is_jwt_valid" => true, "is_subscription_valid" => true, "id_company" => $payload["id_company"], "id_user" => $payload["sub"]] , 200);
            else return response()->json(["is_jwt_valid" => true, "is_subscription_valid" => false, "id_company" => $payload["id_company"], "id_user" => $payload["sub"]] , 200);


        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(["is_jwt_valid" => false, "is_subscription_valid" => false  ] , 200);
        }

    }

    private function sendErrorMessage($code)
    {
        if(null == $this->exceptionFormatter)
            $this->exceptionFormatter = new ExceptionFormatter();
        return $this->exceptionFormatter->sendErrorResponse($code);
    }

}
