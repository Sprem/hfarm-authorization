<?php

namespace App\Http\Controllers\Subscription;

class SubscriptionClient
{

    public function hasValidSubscription($idCompany)
    {

        try {

            $client = new \GuzzleHttp\Client();
            $authorization = "Basic ".base64_encode(env('SUBSCRIPTION_CLIENT_ID').":".env('SUBSCRIPTION_SECRET_ID'));

            $response = $client->request('GET',
                                env('SUBSCRIPTION_URL'), [
                                    'headers' => [
                                        'Authorization' => $authorization
                                    ],
                                    'query' => [
                                        'id_company' => $idCompany
                                    ]
                                ]);

            $jsonBody = json_decode($response->getBody(), true);
            return (!$jsonBody) ? false : true;

        } catch (\Exception $e) {
            throw $e;
        }

    }

}
